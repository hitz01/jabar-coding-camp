/*

    soal no 1

*/

var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// 5 dengan spasi, 5 nya tidak kebawa karena itu batas akhir
let jawab = pertama.substring(0, 5);
jawab += pertama.substring(12, 19);

jawab += kedua.substring(0, 8);
jawab += kedua.substring(8, 19).toUpperCase();

console.log(jawab);

// jawab = saya senang belajar JAVASCRIPT


/*

    soal no 2

*/

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

kataPertama = Number(kataPertama);
kataKedua = Number(kataKedua);
kataKetiga = Number(kataKetiga);
kataKeempat = Number(kataKeempat);

console.log(kataPertama+kataKedua*kataKetiga+kataKeempat);

/*

    soal no 3

*/

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(kalimat.indexOf("javascript"),10);
var kataKetiga = kalimat.substr(kalimat.indexOf("itu"),3); 
var kataKeempat = kalimat.substr(kalimat.indexOf("keren"),5); 
var kataKelima = kalimat.substr(kalimat.indexOf("sekali"),6); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
