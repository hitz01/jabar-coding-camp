// user input preparation
/* 
    prompt ini hanya tersedia oleh node.js tidak untuk browser
    sebelum menggunakan code ini pastikan install prompt-sync
    jika belum terinstall maka lakukan penginstalan dengan menulis command
    npm install prompt-sync
    pre-req : node.js dan npm
*/
const prompt = require('prompt-sync')({sigint: true});

/* 

    soal no 1

*/
var nilai = prompt('masukan nilai: ');
nilai = parseInt(nilai);

// mengecek apakah user sudah memasukan angka atau tidak
while(isNaN(nilai)) {
    nilai = prompt('nilai harus int: ');
    // karena prompt ini return tipe data string (meskipun inputnya int);
    nilai = parseInt(nilai);
}

if((nilai >= 85) && (nilai <= 100)) {console.log('indexnya A');}
else if((nilai >= 75) && (nilai < 85)) {console.log('indexnya B');}
else if((nilai >= 65) && (nilai < 75)) {console.log('indexnya C');}
else if((nilai >= 55) && (nilai < 65)) {console.log('indexnya D');}
else if((nilai < 55) && (nilai >= 0)) {console.log('indexnya E');}
else {console.log('input tidak sesuai (1 - 100)');}

/* 

    soal no 2

*/
console.log();
const tanggal = 8;
let bulan = 4;
const tahun = 2002;

switch(bulan) {
    /* 
    jadi nanti variable bulan akan saya
    redecalre menjadi string (jan,feb,ap)
    */
   case 1: 
        bulan = "Januari";
        break;
    case 2: 
        bulan = "Februari";
        break;
    case 3: 
        bulan = "Maret";
        break;
    case 4: 
        bulan = "April";
        break;
    case 5: 
        bulan = "Mei";
        break;
    case 6: 
        bulan = "Juni";
        break;
    case 7: 
        bulan = "Juli";
        break;
    case 8: 
        bulan = "Agustus";
        break;
    case 9: 
        bulan = "September";
        break;
    case 10: 
        bulan = "Oktober";
        break;
    case 11: 
        bulan = "November";
        break;
    case 12: 
        bulan = "Desember";
        break;   
}
console.log(tanggal + " " + bulan + " " +tahun);



/* 

    soal no 3

*/
console.log();
var n = prompt('masukan berapa banyak baris: ');
n = parseInt(n);

// mengecek apakah user sudah memasukan angka atau tidak
while(isNaN(n)) {
    n = prompt('nilai harus int: ');
    // karena prompt ini return tipe data string (meskipun inputnya int);
    n = parseInt(n);
}
for(let i=0; i<n; i++) {
    for(let j=0; j<=i; j++) {
        // menampilkan tanpa membuat baris baru
        process.stdout.write("#");
    }
    // membuat baris baru
    console.log('');
}


/* 

    soal no 4

*/
console.log();
var m = prompt('masukan Nilai m: ');
m = parseInt(m);

// mengecek apakah user sudah memasukan angka atau tidak
while(isNaN(m)) {
    m = prompt('nilai harus int: ');
    // karena prompt ini return tipe data string (meskipun inputnya int);
    m = parseInt(m);
}
let temp = 1;
for(let i=1;i<=m + (m/3);i++) {
    if(i%4 == 1) {
        console.log(temp + " - I love Programming");
        temp++;
    } 
    else if(i%4 == 2) {
        console.log(temp + " - I love Javascript");
        temp++;
    }
    else if(i%4 == 3) {
        console.log(temp + " - I love VueJS");
        temp++;
    } 
    else if(i%4 == 0) {
        for(let j=1;j<temp;j++)
            process.stdout.write("=");
        console.log();
    }
}