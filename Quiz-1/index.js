/*

function penghasil tanggal hari esok

*/
function next_date(tanggal, bulan, tahun){
    let tahunKabisat;
    let hariPerBulan;
    
    // menentukan tahun kabisat
    if(tahun % 400 == 0) {
        tahunKabisat = true;
    } 
    else if(tahun % 100 == 0) {
        tahunKabisat = false;
    }
    else if(tahun % 4 == 0) {
        tahunKabisat = true;
    }
    else {
        tahunKabisat = false;
    }
    
    // menentukan jumlah hari dalam setiap bulan
    switch(bulan) {
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12: 
            hariPerBulan = 31;
            break;
        
        case 4:
        case 6:
        case 9:
        case 11:
            hariPerBulan = 30;
            break;
    }
    
    tanggal++; // hari esok
    
    // cek apakah bulan februari atau bukan
    if(bulan == 2) {
        if(tahunKabisat) {
            if(tanggal > 29) {
                bulan++;
                tanggal = 1;
            }
        } else {
            if(tanggal > 28) {
                bulan++;
                tanggal = 1;
            }
        }
    }
    
    // selain bulan februari
    if(hariPerBulan == 30) {
        if(tanggal > 30) {
            tanggal = 1;
            bulan++;
        }
    } else if(hariPerBulan == 31) {
        if(tanggal > 31) {
            tanggal = 1;
            bulan++;
        }
    }
    
    if(bulan == 13) {
        bulan = 1;
        tahun++;
    }

    // konversi digit bulan ke kata
    switch(bulan) {
        case 1:
            bulan = "Januari";
            break;
        case 2:
            bulan = "Februari";
            break;
        case 3:
            bulan = "Maret";
            break;
        case 4:
            bulan = "April";
            break;
        case 5:
            bulan = "Mei";
            break;
        case 6:
            bulan = "Juni";
            break;
        case 7:
            bulan = "Juli";
            break;
        case 8:
            bulan = "Agustus";
            break;
        case 9:
            bulan = "September";
            break;
        case 10:
            bulan = "Oktober";
            break;
        case 11:
            bulan = "November";
            break;
        case 12:
            bulan = "Desember";
            break;
    }
    console.log('Tanggal Hari esok: ' + tanggal+' '+bulan+' '+tahun);
}

let tanggal = 31;
let bulan = 12;
let tahun = 2000;

next_date(tanggal,bulan,tahun);


