/*

    soal no 1

*/
// pp = persegi panjang
const luasPp = (p,l) => p*l;
const KelilingPp = (p,l) => 2*(p+l);

console.log(luasPp(3,5));
console.log(KelilingPp(3,5));

/*

    soal no 2

*/
const newFunction = (firstName, lastName) => {
    return {
       firstName,
       lastName,
       fullName: () => console.log(firstName + ' ' + lastName)
    }
}
newFunction("William", "Imoh").fullName() 

/*

    soal no 3

*/
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const {firstName,lastName,address,hobby} = newObject;
console.log(firstName, lastName, address, hobby);

/*

    soal no 4

*/
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]; // ES6
//Driver Code
console.log(combined)

/*

    soal no 5

*/
const planet = "earth" 
const view = "glass" 
// var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
const after = `Lorem ${view} dolor sit amet, consectur adipiscing elit, ${planet}`;
console.log(after);