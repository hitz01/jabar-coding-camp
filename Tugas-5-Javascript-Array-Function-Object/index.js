/*

    soal no 1

*/
console.log("soal no 1");
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
for(let i=0;i<daftarHewan.length;i++) {
    console.log(daftarHewan[i]);
}

/*

    soal no 2

*/
console.log("\nsoal no 2");
let data = {
    name : "Mujahid Ansori Majid",
    age : 19,
    address : "Kota Bandung",
    hobby : "Coding"
}

function introduce (data) {
    console.log("Nama Saya " + data.name +
         ", umur saya " + data.age + 
         ", alamat saya di " + data.address + 
         ", dan saya punya hobby yaitu " + data.hobby
    );
}
let perkenalan = introduce(data);
// tidak perlu lagi console.log(perkenalan), karena dalam function akan ditampilkan

/*

    soal no 3

*/
console.log("\nsoal no 3");
function hitung_huruf_vokal(sebuahString) {
    // agar tidak mempermasalahkan besar kecilnya huruf
    sebuahString = sebuahString.toLowerCase();
    // agar lebih efisien, spasi dihilangkan dengan
    sebuahString = sebuahString.replace(/\s/g, '');
    
    // penampung total huruf vokal
    let total = 0;
    for(let i=0;i<sebuahString.length;i++) {
        if(sebuahString[i] === 'a' || 
            sebuahString[i] === 'i' || 
            sebuahString[i] === 'u' || 
            sebuahString[i] === 'e' || 
            sebuahString[i] === 'o') {
                total++;
            }
            
    }
    return total;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

/*

    soal no 4

*/
console.log("\nsoal no 4");
function hitung(angka) {
    return angka*2-2;
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
